﻿namespace BookKeeping.Web.Components.Notifier.Model
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Notifier. To display system messages to the user
    /// </summary>
    public interface INotifier
    {
        /// <summary>
        /// Collection of error messages
        /// </summary>
        IEnumerable<Message> Errors { get; }

        /// <summary>
        /// Collection of information messages
        /// </summary>
        IEnumerable<Message> Messages { get; }

        /// <summary>
        /// Collection of warning messages
        /// </summary>
        IEnumerable<Message> Warnings { get; }

        /// <summary>
        /// Gets all current messages for user
        /// </summary>
        /// <returns>All messages</returns>
        IEnumerable<Message> GetAllEntries();

        /// <summary>
        /// Add new message for user
        /// </summary>
        /// <param name="type">Message type</param>
        /// <param name="message">Message text</param>
        void Add(NotifyType type, string message);

        /// <summary>
        /// Clear current list of messages
        /// </summary>
        void Clear();
    }

    internal class Notifier : INotifier
    {
        private readonly IList<Message> entries;

        public Notifier()
        {
            this.entries = new List<Message>();
        }

        public IEnumerable<Message> Errors
        {
            get
            {
                return this.entries.Where(p => p.Type == NotifyType.Error);
            }
        }

        public IEnumerable<Message> Messages
        {
            get
            {
                return this.entries.Where(p => p.Type == NotifyType.Information);
            }
        }

        public IEnumerable<Message> Warnings
        {
            get
            {
                return this.entries.Where(p => p.Type == NotifyType.Warning);
            }
        }

        public void Add(NotifyType type, string message)
        {
            this.entries.Add(new Message { Type = type, MessageText = message });
        }

        public void Clear()
        {
            this.entries.Clear();
        }

        public IEnumerable<Message> GetAllEntries()
        {
            return this.entries;
        }
    }
}