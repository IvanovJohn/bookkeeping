namespace BookKeeping.Web.Components.Notifier.Model
{
    /// <summary>
    ///  Message
    /// </summary>
    public class Message 
    {
        /// <summary>
        /// Type of notification 
        /// </summary>
        public NotifyType Type { get; set; }
        
        /// <summary>
        /// Message text
        /// </summary>
        public string MessageText { get; set; }
    }
}