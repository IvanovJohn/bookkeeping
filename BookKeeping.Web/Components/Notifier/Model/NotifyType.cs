namespace BookKeeping.Web.Components.Notifier.Model
{   
    /// <summary>
    /// Type of notification 
    /// </summary>
    public enum NotifyType
    { 
        /// <summary>
        /// Information
        /// </summary>
        Information = 0,
        
        /// <summary>
        /// Warning
        /// </summary>
        Warning = 1,

        /// <summary>
        /// Error
        /// </summary>
        Error = 2
    }
}