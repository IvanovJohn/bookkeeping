namespace BookKeeping.Web.Components.Notifier.Model
{
    using System;

    /// <summary>
    /// Extentions methods for <see cref="INotifier"/>
    /// </summary>
    public static class NotifierExtensions
    {
        /// <summary>
        /// Add information message to notifier
        /// </summary>
        /// <param name="notifier">Notifier <see cref="INotifier"/></param>
        /// <param name="message">Message text</param>
        public static void Information(this INotifier notifier, string message)
        {
            notifier.Add(NotifyType.Information, message);
        }

        /// <summary>
        /// Add warning message to notifier
        /// </summary>
        /// <param name="notifier">Notifier <see cref="INotifier"/></param>
        /// <param name="message">Message text</param>
        public static void Warning(this INotifier notifier, string message)
        {
            notifier.Add(NotifyType.Warning, message);
        }

        /// <summary>
        /// Add error message to notifier
        /// </summary>
        /// <param name="notifier">Notifier <see cref="INotifier"/></param>
        /// <param name="message">Message text</param>
        public static void Error(this INotifier notifier, string message)
        {
            notifier.Add(NotifyType.Error, message);
        }

        /// <summary>
        /// Add error message to notifier from exception
        /// </summary>
        /// <param name="notifier">Notifier <see cref="INotifier"/></param>
        /// <param name="exception">Exception</param>
        public static void Error(this INotifier notifier, Exception exception)
        {
            notifier.Add(NotifyType.Error, exception.Message);
        }
    }
}