﻿$(document).ready(function() {
    $(document).ajaxStart(function() {
        clearMessages();
    });

    $(document).ajaxError(function(event, jqXhr) {
        if (jqXhr.status != '500') {
            return;
        }
        var jsonValue = jQuery.parseJSON(jqXhr.responseText);
        addMessages(jsonValue);
    });

    $(document).ajaxComplete(function(event, jqXhr) {
        try {
            var jsonValue = jQuery.parseJSON(jqXhr.responseText);
            if (!jsonValue) {
                return;
            }
            if (jsonValue.notifications) {
                addMessages(jsonValue.notifications);
            }
        } catch(e) {
        }
    });

    function getNotifier() {
        return $('div.notifier');
    }
    function clearMessages() {
        var notifier = getNotifier();
        notifier.empty();
    }

    function addMessages(messages) {
        try {
            var getNotifyContainer = function(notifier, type) {
                var css = '';
                if (type == 2) {
                    css = 'alert-danger';
                }

                if (type == 1) {
                    css = 'alert-warning';
                }

                if (type == 0) {
                    css = 'alert-success';
                }

                var result = notifier.find('div.' + css);
                if (result.length == 0) {
                    notifier.append('<div class="alert ' + css + '"><ul></ul></div>');
                }
                return notifier.find('div.' + css + ' ul');
            };

            var notifierDiv = $('div.notifier');
            for (var i = 0; i < messages.length; i++) {
                var ul = getNotifyContainer(notifierDiv, messages[i].Type);
                var li = '<li>' + messages[i].MessageText + '</li>';
                ul.append(li);
            }
        } catch(e) {            
        }
    }
});