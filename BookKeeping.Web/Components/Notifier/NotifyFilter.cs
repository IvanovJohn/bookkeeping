﻿namespace BookKeeping.Web.Components.Notifier
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using BookKeeping.Web.Components.Notifier.Model;

    internal class NotifyFilter : IActionFilter, IExceptionFilter
    {
        private INotifier Notifier
        {
            get
            {
                return DependencyResolver.Current.GetService<INotifier>();
            }
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAjaxRequest())
            {
                return;
            }

            var modeState = filterContext.Controller.ViewData.ModelState;
            if (!modeState.IsValid)
            {
                filterContext.Result = this.GenerateJsonResult(modeState);                
            }
        }

        /// <summary>
        /// Called after the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var jsonResult = filterContext.Result as JsonResult;

            if (jsonResult == null)
            {
                return;
            }

            if (jsonResult.Data == null)
            {
                var modeState = filterContext.Controller.ViewData.ModelState;
                filterContext.Result = this.GenerateJsonResult(modeState);
            }
        }

        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAjaxRequest())
            {
                return;
            }
            
            this.Notifier.Error(filterContext.Exception);

            var modeState = filterContext.Controller.ViewData.ModelState;
            filterContext.Result = this.GenerateJsonResult(modeState);

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
            filterContext.HttpContext.Response.StatusCode = 500;                     
        }

        private IEnumerable<Message> GetAllEntries(IEnumerable<KeyValuePair<string, ModelState>> modelState)
        {
            var modelStateErrors = modelState
                .SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage))
                .Select(p => new Message { MessageText = p, Type = NotifyType.Error });
            var result = this.Notifier.GetAllEntries().Union(modelStateErrors).ToList();
            this.Notifier.Clear();
            return result;
        }

        private JsonResult GenerateJsonResult(IEnumerable<KeyValuePair<string, ModelState>> modelState)
        {
            var result = new JsonResult();
            result.Data = new { notifications = this.GetAllEntries(modelState) };

            return result;
        }
    }
}