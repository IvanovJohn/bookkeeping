﻿namespace BookKeeping.Web.Components.Notifier
{
    using System.Web.Mvc;
    using System.Web.Mvc.Html;

    using BookKeeping.Web.Components.Notifier.Model;

    /// <summary>
    /// Utility class for rendering notifier messages
    /// </summary>
    public static class NotifierHtmlHelper
    {
        /// <summary>
        /// Render notifier
        /// </summary>
        /// <param name="htmlHelper">Html helper</param>
        /// <returns>Rendered html string</returns>
        public static MvcHtmlString Notifier(this HtmlHelper htmlHelper)
        {            
            var notifier = DependencyResolver.Current.GetService<INotifier>();

            var result = htmlHelper.Partial("~/Components/Notifier/Views/Notifier.cshtml", notifier);
            notifier.Clear();
            return result;
        }
    }
}