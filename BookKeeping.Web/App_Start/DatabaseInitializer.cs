﻿namespace BookKeeping.Web
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    using BookKeeping.Web.Models.DAL;

    using WebMatrix.WebData;

    /// <summary>
    /// Database initializer
    /// </summary>
    public class DatabaseInitializer
    {
        /// <summary>
        /// Initialize database
        /// </summary>
        public static void Initialise()
        {
            Database.SetInitializer<BookKeepingDataContext>(null);

            try
            {
                using (var context = new BookKeepingDataContext())
                {
                    if (!context.Database.Exists())
                    {                        
                        ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();
                    }
                }

                WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex);
            }
        }
    }
}