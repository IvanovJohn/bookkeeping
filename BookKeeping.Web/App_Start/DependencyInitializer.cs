namespace BookKeeping.Web
{
    using System;
    using System.Web;
    using System.Web.Mvc;

    using BookKeeping.Web.Components.Notifier.Model;
    using BookKeeping.Web.Models.DAL;
    using BookKeeping.Web.Models.Services;

    using Microsoft.Practices.Unity;

    using Unity.Mvc4;

    /// <summary>
    /// DI inicializer
    /// </summary>
    public static class DependencyInitializer
    {
        /// <summary>
        /// Initialize dependencies in application
        /// </summary>
        public static void Initialise()
        {
            var container = BuildUnityContainer(); 
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));            
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            RegisterTypes(container);

            return container;
        }

        private static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<DbContextProvider, DbContextProvider>(new PerRequestLifetimeManager());

            container.RegisterType<IAccountService, AccountService>();
            container.RegisterType<IOperationService, OperationService>();

            container.RegisterType<INotifier, Notifier>(new UnityPerSessionLifetimeManager("Notifier"));            
        }

        private class UnityPerSessionLifetimeManager : LifetimeManager
        {
            private readonly string sessionKey;

            public UnityPerSessionLifetimeManager(string sessionKey)
            {
                this.sessionKey = sessionKey;
            }

            public override object GetValue()
            {
                return HttpContext.Current.Session[this.sessionKey];
            }

            public override void RemoveValue()
            {
                HttpContext.Current.Session.Remove(this.sessionKey);
            }

            public override void SetValue(object newValue)
            {
                HttpContext.Current.Session[this.sessionKey] = newValue;
            }
        }

        private class PerRequestLifetimeManager : LifetimeManager, IDisposable
        {
            #region Constants and Fields
            
            private readonly Guid key = Guid.NewGuid();

            #endregion

            #region Public Methods
            
            public void Dispose()
            {
                this.RemoveValue();
            }
           
            public override object GetValue()
            {
                return HttpContext.Current.Items[this.key];
            }
            
            public override void RemoveValue()
            {
                HttpContext.Current.Items.Remove(this.key);
            }
           
            public override void SetValue(object newValue)
            {
                HttpContext.Current.Items[this.key] = newValue;
            }

            #endregion
        }
    }
}