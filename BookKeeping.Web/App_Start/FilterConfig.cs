﻿namespace BookKeeping.Web
{    
    using System.Web.Mvc;

    using BookKeeping.Web.Components.Notifier;    

    /// <summary>
    /// Utility class for filters registration
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// Register global filters
        /// </summary>
        /// <param name="filters">Collection of filters</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            filters.Add(new NotifyFilter());
        }
    }
}