namespace BookKeeping.Web
{
    using System.Web.Optimization;

    /// <summary>
    /// Config for bunldles of bootstrap
    /// </summary>
    public class BootstrapBundleConfig
    {
        /// <summary>
        /// Register bundles
        /// </summary>        
        public static void RegisterBundles()
        {            
            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.js"));
            BundleTable.Bundles.Add(
                new StyleBundle("~/Content/bootstrap/base").Include("~/Content/bootstrap/bootstrap.css"));
            BundleTable.Bundles.Add(
                new StyleBundle("~/Content/bootstrap/theme").Include("~/Content/bootstrap/bootstrap-theme.css"));
        }
    }
}
