﻿namespace BookKeeping.Web
{    
    using System.Web.Optimization;

    /// <summary>
    /// Bundle config
    /// </summary>
    public class BundleConfig
    {        
        /// <summary>
        /// Register bundles
        /// </summary>
        /// <param name="bundles">Collextions of bundles</param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                     "~/Scripts/jquery.unobtrusive*",
                     "~/Scripts/jquery.validate*",
                     "~/Scripts/app/bootstrap-validation*"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                        "~/Scripts/knockout-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/scriptsForAllPages").Include(
                "~/Scripts/app/ajaxForm.js",
                "~/Components/Notifier/Scripts/notifier.js",
                "~/Scripts/jquery-ui-1.10.3.min.js",
                "~/Scripts/typeahead.min.js",
                "~/Scripts/knockout-2.3.0.js",
                "~/Scripts/app/knockoutBindings.js",
                "~/Scripts/app/GridView.js"));            
            
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/main.css",
                "~/Content/themes/base/minified/jquery-ui.min.css",
                "~/Content/themes/base/minified/jquery.ui.datepicker.min.css"));       
        }
    }
}