﻿namespace BookKeeping.Web.ViewModel.Operation
{
    using System;
    using System.Collections.Generic;

    using BookKeeping.Web.Models.Domain;

    /// <summary>
    /// View model for list of operations
    /// </summary>
    public class OperationListViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OperationListViewModel"/> class.
        /// </summary>
        public OperationListViewModel()
        {
            this.Paging = new PagingData();
            this.Filter = new OperationFilter
                              {
                                  Month = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
                              };
        }

        /// <summary>
        /// Collection of rows
        /// </summary>
        public IEnumerable<OperationViewModel> Data { get; set; }

        /// <summary>
        /// Information about paging data
        /// </summary>
        public PagingData Paging { get; set; }

        /// <summary>
        /// Operation filter
        /// </summary>
        public OperationFilter Filter { get; set; }        
    }
}