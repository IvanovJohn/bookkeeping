namespace BookKeeping.Web.ViewModel.Operation
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// View model for operation
    /// </summary>
    public class OperationViewModel
    {
        /// <summary>
        /// Operation identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of operation category
        /// </summary>
        [Required]
        [UIHint("OperationCategory")]
        public string CategoryName { get; set; }

        /// <summary>
        /// Date of operation
        /// </summary>
        [DataType(DataType.Date)]        
        public string DateTime { get; set; }
        
        /// <summary>
        /// Operation value
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// Comment for operation
        /// </summary>
        [DataType(DataType.MultilineText)]
        public string Comment { get; set; }
    }
}