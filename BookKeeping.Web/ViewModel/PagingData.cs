namespace BookKeeping.Web.ViewModel
{
    using System;

    /// <summary>
    /// Information about paging for lists
    /// </summary>
    public class PagingData
    {
        private const int NumberRowsOnPage = 10;        

        /// <summary>
        /// Initializes a new instance of the <see cref="PagingData"/> class.
        /// </summary>
        public PagingData()
        {
            this.PageNumber = 1;
        }

        /// <summary>
        /// Current page number
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Total pages count
        /// </summary>
        public int TotalPagesCount
        {
            get
            {
                return (int)Math.Ceiling((decimal)this.TotalCount / NumberRowsOnPage);
            }
        }

        /// <summary>
        /// Total rows count
        /// </summary>
        public int TotalCount { get; set; }
    }
}