namespace BookKeeping.Web.ViewModel.Account
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// View model for login action
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// Login
        /// </summary>
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// Remember me checkbox
        /// </summary>
        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}