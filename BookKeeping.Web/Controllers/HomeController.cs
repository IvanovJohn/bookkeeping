﻿namespace BookKeeping.Web.Controllers
{
    using System.Web.Mvc;

    /// <summary>
    /// Default controller
    /// </summary>
    [Authorize]
    public class HomeController : Controller
    {
        /// <summary>
        /// Default page
        /// </summary>
        /// <returns><see cref="ActionResult"/></returns>
        public ActionResult Index()
        {            
            return this.RedirectToAction("Index", "Operation");
        }
    }
}