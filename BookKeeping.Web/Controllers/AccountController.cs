﻿namespace BookKeeping.Web.Controllers
{
    using System.Web.Mvc;

    using BookKeeping.Web.Components.Notifier.Model;
    using BookKeeping.Web.Models.Services;
    using BookKeeping.Web.ViewModel.Account;

    /// <summary>
    /// The account controller.
    /// </summary>
    [Authorize]    
    public class AccountController : Controller
    {
        private readonly IAccountService accountService;

        private readonly INotifier notifier;
     
        /// <summary>
        /// Initialize a new instance of <see cref="AccountController"/> class
        /// </summary>
        /// <param name="accountService">Account service</param>
        /// <param name="notifier">Notifier</param>
        public AccountController(IAccountService accountService, INotifier notifier)
        {
            this.accountService = accountService;
            this.notifier = notifier;
        }

        /// <summary>
        /// GET: /Account/Logon
        /// </summary>
        /// <returns><see cref="ActionResult"/></returns>
        [AllowAnonymous]
        public ActionResult Logon()
        {            
            return this.View();
        }

        /// <summary>
        /// POST: /Account/JsonLogin
        /// </summary>
        /// <param name="model">Login model</param>
        /// <param name="returnUrl">Return url</param>
        /// <returns><see cref="JsonResult"/></returns>
        [AllowAnonymous]
        [HttpPost]
        public JsonResult JsonLogin(LoginModel model, string returnUrl)
        {
            this.accountService.Login(model.UserName, model.Password, model.RememberMe);

            if (string.IsNullOrEmpty(returnUrl))
            {
                returnUrl = this.Url.Action("Index", "Home");
            }

            return this.Json(new { success = true, redirect = returnUrl });           
        }
        
        /// <summary>
        /// POST: /Account/LogOff
        /// </summary>
        /// <returns><see cref="ActionResult"/></returns>
        [HttpPost]        
        public ActionResult LogOff()
        {
            this.accountService.Logout();
            return this.RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// POST: /Account/JsonRegister
        /// </summary>
        /// <param name="model">Register model</param>
        /// <param name="returnUrl">Return url</param>
        /// <returns><see cref="ActionResult"/></returns>
        [HttpPost]
        [AllowAnonymous]        
        public JsonResult JsonRegister(RegisterModel model, string returnUrl)
        {
            this.accountService.Register(model.UserName, model.Password);
            if (string.IsNullOrEmpty(returnUrl))
            {
                returnUrl = this.Url.Action("Index", "Home");
            }

            return this.Json(new { success = true, redirect = returnUrl });            
        }

        /// <summary>
        /// GET: /Account/ChangePassword
        /// </summary>        
        /// <returns><see cref="ActionResult"/></returns>
        public ActionResult ChangePassword()
        {                                  
            return this.View();
        }

        /// <summary>
        /// POST: /Account/ChangePassword
        /// </summary>
        /// <param name="model">Change password model</param>
        /// <returns><see cref="ActionResult"/></returns>
        [HttpPost]        
        public JsonResult ChangePassword(ChangePasswordModel model)
        {
            this.accountService.ChangePassword(model.OldPassword, model.NewPassword);
            this.notifier.Information("Your password has been changed.");
            return this.Json(new { success = true }); 
        }        
    }
}