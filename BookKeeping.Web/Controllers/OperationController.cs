namespace BookKeeping.Web.Controllers
{
    using System;
    using System.Collections.Generic;    
    using System.Linq;    
    using System.Web.Mvc;

    using BookKeeping.Web.Components.Notifier.Model;
    using BookKeeping.Web.Filters;
    using BookKeeping.Web.Models.DAL;
    using BookKeeping.Web.Models.Domain;
    using BookKeeping.Web.Models.Services;
    using BookKeeping.Web.ViewModel;
    using BookKeeping.Web.ViewModel.Operation;

    /// <summary>
    /// Operation controller
    /// </summary>
    [Authorize]
    public class OperationController : Controller
    {
        private const int NumberRowsOnPage = 10;

        private readonly IAccountService accountService;
        private readonly INotifier notifier;
        private readonly IOperationService operationService;

        private readonly DbContextProvider dbContextProvider;

        /// <summary>
        /// Initialize a new instance of <see cref="OperationController"/> class
        /// </summary>
        /// <param name="accountService">Account service</param>
        /// <param name="notifier">Notifier</param>
        /// <param name="operationService">Operation service</param>
        /// <param name="dbContextProvider">Data context provider</param>
        public OperationController(IAccountService accountService, INotifier notifier, IOperationService operationService, DbContextProvider dbContextProvider)
        {
            this.accountService = accountService;
            this.notifier = notifier;
            this.operationService = operationService;
            this.dbContextProvider = dbContextProvider;
        }

        /// <summary>
        /// Operations list page
        /// </summary>
        /// <returns><see cref="ActionResult"/></returns>
        public ActionResult Index()
        {
            var viewModel = new OperationListViewModel();
            
            return this.View(viewModel);
        }

        /// <summary>
        /// Add operation dialog
        /// </summary>
        /// <returns><see cref="ActionResult"/></returns>
        [NoCache]
        public ActionResult Add()
        {
            var viewModel = new OperationViewModel();
            viewModel.DateTime = DateTime.Now.ToShortDateString();
            return this.PartialView(viewModel);
        }

        /// <summary>
        /// Save new operation action
        /// </summary>
        /// <param name="newOperationViewModel">New operation view model</param>
        /// <returns><see cref="JsonResult"/></returns>
        public JsonResult SaveNew(OperationViewModel newOperationViewModel)
        {
            using (var dbContext = this.dbContextProvider.Init())
            {
                var currentUser = this.accountService.GetCurrentUser();

                currentUser.CreateNewOperation(
                    newOperationViewModel.CategoryName,
                    newOperationViewModel.Value,
                    DateTime.Parse(newOperationViewModel.DateTime),
                    newOperationViewModel.Comment);

                dbContext.SaveChanges();
                return this.Json(new { success = true });
            }
        }

        /// <summary>
        /// Get all category of operations for current user
        /// </summary>
        /// <returns><see cref="JsonResult"/></returns>
        [NoCache]
        public JsonResult GetAllCategoryOperation()
        {
            using (this.dbContextProvider.Init())
            {
                var currentUser = this.accountService.GetCurrentUser();
                return
                    this.Json(
                        currentUser.OperationCategories.Select(p => new { @value = p.Title }).OrderBy(p => p.value),
                        JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Get one page of operations
        /// </summary>
        /// <param name="filter">Filter for list</param>
        /// <param name="pageNumber">Page number</param>
        /// <returns><see cref="JsonResult"/></returns>
        public JsonResult List(OperationFilter filter, int pageNumber)
        {
            using (this.dbContextProvider.Init())
            {
                var operationsQuery = this.operationService.GetListOfOperationsForCurrentUser(filter);
                operationsQuery = operationsQuery.OrderByDescending(p => p.DateTime);

                var resut = new OperationListViewModel
                                {
                                    Paging =
                                        new PagingData
                                            {
                                                PageNumber = pageNumber,
                                                TotalCount = operationsQuery.Count()
                                            }
                                };

                if (resut.Paging.TotalCount > 0)
                {
                    resut.Data =
                        operationsQuery.Skip((resut.Paging.PageNumber - 1) * NumberRowsOnPage)
                            .Take(NumberRowsOnPage)
                            .Select(CreateViewModel())
                            .ToList();
                }
                else
                {
                    resut.Data = new List<OperationViewModel>();
                }

                return this.Json(resut);
            }
        }

        /// <summary>
        /// Delete operations
        /// </summary>
        /// <param name="id">Collection of identifiers of removing operaitions</param>
        /// <returns><see cref="JsonResult"/></returns>
        public JsonResult Delete(IEnumerable<int> id)
        {
            using (var dbContext = this.dbContextProvider.Init())
            {
                this.operationService.RemoveOperations(id);
                this.notifier.Information("Operations removed");
                dbContext.SaveChanges();
                return this.Json(null);
            }
        }

        private static Func<Operation, OperationViewModel> CreateViewModel()
        {
            return
                p =>
                new OperationViewModel
                    {
                        CategoryName = p.Category.Title,
                        Comment = p.Comment,
                        DateTime = p.DateTime.ToShortDateString(),
                        Id = p.Id,
                        Value = p.Value
                    };
        }
    }
}