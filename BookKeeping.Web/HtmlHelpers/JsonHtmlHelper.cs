namespace BookKeeping.Web.HtmlHelpers
{
    using System.Web.Mvc;
    using System.Web.Script.Serialization;

    /// <summary>
    /// Utility class for work with JSON
    /// </summary>
    public static class JsonHtmlHelper
    {
        /// <summary>
        /// Render object as JSON string
        /// </summary>
        /// <param name="html">Html helper</param>
        /// <param name="obj">Rendering object</param>
        /// <returns>Rendered html string</returns>
        public static MvcHtmlString ToJson(this HtmlHelper html, object obj)
        {
            var serializer = new JavaScriptSerializer();
            return MvcHtmlString.Create(serializer.Serialize(obj));
        }
    }
}