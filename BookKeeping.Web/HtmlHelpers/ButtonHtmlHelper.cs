﻿namespace BookKeeping.Web.HtmlHelpers
{
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    /// Utility class for render buttons
    /// </summary>
    public static class ButtonHtmlHelper
    {
        /// <summary>
        /// Render button
        /// </summary>
        /// <param name="helper">Html helper</param>
        /// <param name="text">Button text</param>
        /// <param name="cssClass">Button css class</param>
        /// <param name="isSubmit">Button is submit</param>
        /// <param name="attributesObject">Addititional attributes</param>
        /// <returns>Rendered html string</returns>
        public static MvcHtmlString Button(this HtmlHelper helper, string text, string cssClass = null, bool isSubmit = false, object attributesObject = null)
        {
            var tagBuilder = new TagBuilder("input");

            if (!string.IsNullOrEmpty(cssClass))
            {
                tagBuilder.AddCssClass(cssClass);
            }

            tagBuilder.AddCssClass("btn");
            if (isSubmit)
            {
                tagBuilder.Attributes.Add("type", "submit");
                tagBuilder.AddCssClass("btn-primary");
            }
            else
            {
                tagBuilder.Attributes.Add("type", "button");
                tagBuilder.AddCssClass("btn-default");
            }

            tagBuilder.MergeAttributes(new RouteValueDictionary(attributesObject));

            tagBuilder.Attributes.Add("value", text);
            
            return new MvcHtmlString(tagBuilder.ToString(TagRenderMode.SelfClosing));
        }
    }
}