﻿namespace BookKeeping.Web.Models.Services
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using BookKeeping.Web.Models.DAL;
    using BookKeeping.Web.Models.Domain;        

    /// <summary>
    /// Service for work with <see cref="Operation"/>
    /// </summary>
    public interface IOperationService
    {
        /// <summary>
        /// Get query for list of filtered operations for current user
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <returns><see cref="IQueryable{Operation}"/></returns>
        IQueryable<Operation> GetListOfOperationsForCurrentUser(OperationFilter filter);

        /// <summary>
        /// Remove operations
        /// </summary>
        /// <param name="operationIds">Ids of operations</param>
        void RemoveOperations(IEnumerable<int> operationIds);
    }

    internal class OperationService : AbstractDomainService, IOperationService
    {     
        private readonly IAccountService accountService;
        
        public OperationService(DbContextProvider dbContextProvider, IAccountService accountService)
            : base(dbContextProvider)
        {        
            this.accountService = accountService;
        }

        public IQueryable<Operation> GetListOfOperationsForCurrentUser(OperationFilter filter)
        {
            var currentUser = this.accountService.GetCurrentUser();
            var operationsQuery = this.DataContext.Operations.Where(p => p.UserProfile.UserId == currentUser.UserId);

            var endOfMonth = filter.Month.AddMonths(1);
            operationsQuery = operationsQuery.Where(p => p.DateTime >= filter.Month && p.DateTime < endOfMonth);

            if (!string.IsNullOrEmpty(filter.CategoryStartsWith))
            {
                operationsQuery = operationsQuery.Where(p => p.Category.Title.StartsWith(filter.CategoryStartsWith));
            }

            return operationsQuery.Include(p => p.Category);
        }

        public void RemoveOperations(IEnumerable<int> operationIds)
        {
            var currentUser = this.accountService.GetCurrentUser();
            var deletingoperations = currentUser.Operations.Where(p => operationIds.Contains(p.Id)).ToList();

            foreach (var deletingoperation in deletingoperations)
            {
                this.DataContext.Operations.Remove(deletingoperation);
            }

            this.DataContext.SaveChanges();            
        }
    }
}