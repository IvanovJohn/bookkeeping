﻿namespace BookKeeping.Web.Models.Services
{    
    using BookKeeping.Web.Models.DAL;

    internal abstract class AbstractDomainService
    {
        private readonly DbContextProvider dbContextProvider;

        protected AbstractDomainService(DbContextProvider dbContextProvider)
        {
            this.dbContextProvider = dbContextProvider;
        }      

        protected BookKeepingDataContext DataContext
        {
            get
            {
                return this.dbContextProvider.CurrentContext;
            }
        }
    }
}