﻿namespace BookKeeping.Web.Models.Services
{
    using System;
    using System.Linq;
    using System.Security.Authentication;
    using System.Web.Security;

    using BookKeeping.Web.Models.DAL;
    using BookKeeping.Web.Models.Domain;

    using WebMatrix.WebData;
    
    /// <summary>
    /// Authentification service
    /// </summary>
    public interface IAccountService
    {
        /// <summary>
        /// Check login user information and create auth ticket (in cookie) if this correct
        /// </summary>
        /// <param name="userName">Username</param>
        /// <param name="password">Passworrd</param>
        /// <param name="persistCookie">create persisted cookie</param>
        void Login(string userName, string password, bool persistCookie);

        /// <summary>
        /// Logout current user
        /// </summary>
        void Logout();

        /// <summary>
        /// Register new user
        /// </summary>
        /// <param name="userName">Username</param>
        /// <param name="password">Password</param>
        void Register(string userName, string password);

        /// <summary>
        /// Change pasword for current user
        /// </summary>
        /// <param name="oldPassword">Old password</param>
        /// <param name="newPassword">New password</param>
        void ChangePassword(string oldPassword, string newPassword);

        /// <summary>
        /// Returns current user
        /// </summary>
        /// <returns>Current user</returns>
        UserProfile GetCurrentUser();
    }

    internal class AccountService : AbstractDomainService, IAccountService
    {
        public AccountService(DbContextProvider dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void Login(string userName, string password, bool persistCookie)
        {
            if (WebSecurity.Login(userName, password, persistCookie))
            {
                FormsAuthentication.SetAuthCookie(userName, persistCookie);
                return;
            }

            throw new AuthenticationException("The user name or password provided is incorrect.");
        }

        /// <summary>
        /// Logout current user
        /// </summary>
        public void Logout()
        {
            WebSecurity.Logout();
        }

        public void Register(string userName, string password)
        {
            // Attempt to register the user
            try
            {
                WebSecurity.CreateUserAndAccount(userName, password);
                WebSecurity.Login(userName, password);
                FormsAuthentication.SetAuthCookie(userName, false);                
            }
            catch (MembershipCreateUserException e)
            {
                throw new Exception(ErrorCodeToString(e.StatusCode));
            }
        }

        public void ChangePassword(string oldPassword, string newPassword)
        {
            if (!WebSecurity.ChangePassword(WebSecurity.CurrentUserName, oldPassword, newPassword))
            {
                throw new Exception("The current password is incorrect or the new password is invalid.");
            }
        }

        public UserProfile GetCurrentUser()
        {
            return this.DataContext.UserProfiles.First(p => p.UserName == WebSecurity.CurrentUserName);
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }              
    }
}