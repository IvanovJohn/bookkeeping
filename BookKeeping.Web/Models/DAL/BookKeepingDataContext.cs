namespace BookKeeping.Web.Models.DAL
{
    using System.Data.Entity;

    using BookKeeping.Web.Models.Domain;

    /// <summary>
    ///  Application datacontext
    /// </summary>
    public class BookKeepingDataContext : DbContext
    {
        /// <summary>
        /// Initialize a new instance of <see cref="BookKeepingDataContext"/> class
        /// </summary>
        public BookKeepingDataContext()
            : base("DefaultConnection")
        {
        }

        /// <summary>
        /// Set of <see cref="UserProfile"/>
        /// </summary>
        public DbSet<UserProfile> UserProfiles { get; set; }

        /// <summary>
        /// Set of <see cref="OperationCategory"/>
        /// </summary>
        public DbSet<OperationCategory> OperationCategories { get; set; }

        /// <summary>
        /// Set of <see cref="Operation"/>
        /// </summary>
        public DbSet<Operation> Operations { get; set; }

        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        ///                 before the model has been locked down and used to initialize the context.  The default
        ///                 implementation of this method does nothing, but it can be overridden in a derived class
        ///                 such that the model can be further configured before it is locked down.
        /// </summary>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        ///                 is created.  The model for that context is then cached and is for all further instances of
        ///                 the context in the app domain.  This caching can be disabled by setting the ModelCaching
        ///                 property on the given ModelBuidler, but note that this can seriously degrade performance.
        ///                 More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        ///                 classes directly.
        /// </remarks>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserProfile>().HasMany(p => p.OperationCategoriesInternal).WithOptional().WillCascadeOnDelete();
            modelBuilder.Entity<UserProfile>().HasMany(p => p.OperationsInternal);                
        }
    }
}