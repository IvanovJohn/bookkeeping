﻿namespace BookKeeping.Web.Models.DAL
{
    using System;    
    
    /// <summary>
    /// Data context provider
    /// </summary>
    public class DbContextProvider
    {
        /// <summary>
        /// Current data context
        /// </summary>
        public BookKeepingDataContext CurrentContext { get; private set; }

        /// <summary>
        /// Initialize current context
        /// </summary>
        /// <returns>Initialized context</returns>
        public BookKeepingDataContext Init()
        {
            if (this.CurrentContext != null)
            {
                throw new Exception("BookKeepingDataContext already created");
            }

            this.CurrentContext = new BookKeepingDataContext();

            return this.CurrentContext;
        }       
    }    
}