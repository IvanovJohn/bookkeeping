namespace BookKeeping.Web.Models.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// User profile
    /// </summary>
    [Table("UserProfile")]
    public class UserProfile
    {        
        /// <summary>
        /// Identifier
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Collections of user's operations
        /// </summary>
        public IEnumerable<Operation> Operations
        {
            get
            {
                return this.OperationsInternal;
            }            
        }

        /// <summary>
        /// Collections of user's operation categories
        /// </summary>
        public IEnumerable<OperationCategory> OperationCategories
        {
            get
            {
                return this.OperationCategoriesInternal;
            }
        }
        
        /// <summary>
        /// Internal list for operations db mapping
        /// </summary>
        protected internal virtual ICollection<Operation> OperationsInternal { get; set; }

        /// <summary>
        /// Internal list for categories db mapping
        /// </summary>
        protected internal virtual ICollection<OperationCategory> OperationCategoriesInternal { get; set; }

        /// <summary>
        /// Creates new user expense
        /// </summary>
        /// <param name="categoryName">Operation category</param>
        /// <param name="sum">Sum of money</param>
        /// <param name="dateTime">Operation date and time</param>
        /// <param name="comment">Comment for operation</param>
        public void CreateNewOperation(string categoryName, decimal sum, DateTime dateTime, string comment)
        {
            var operationCategory = this.GetExistingOrCreateNewCategory(categoryName);
            var operation = new Operation
                                {
                                    Category = operationCategory,
                                    Comment = comment,
                                    DateTime = dateTime,                                    
                                    Value = sum
                                };

            this.OperationsInternal.Add(operation);
        }

        private OperationCategory GetExistingOrCreateNewCategory(string categoryName)
        {
            var existingCategory = this.OperationCategories.FirstOrDefault(p => p.Title == categoryName);
            if (existingCategory == null)
            {
                existingCategory = new OperationCategory { Title = categoryName };
                this.OperationCategoriesInternal.Add(existingCategory);
            }

            return existingCategory;
        }
    }
}