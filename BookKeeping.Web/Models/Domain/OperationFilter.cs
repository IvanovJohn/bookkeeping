﻿namespace BookKeeping.Web.Models.Domain
{
    using System;

    /// <summary>
    /// Operation filter
    /// </summary>
    public class OperationFilter
    {
        /// <summary>
        /// Selected month
        /// </summary>
        public DateTime Month { get; set; }

        /// <summary>
        /// Filter by category
        /// </summary>
        public string CategoryStartsWith { get; set; }
    }
}