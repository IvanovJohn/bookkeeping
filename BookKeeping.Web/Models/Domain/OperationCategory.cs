namespace BookKeeping.Web.Models.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Operation category
    /// </summary>
    public class OperationCategory
    {
        /// <summary>
        /// Identifier
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Title of category
        /// </summary>
        [Required]
        public string Title { get; set; }                
    }
}