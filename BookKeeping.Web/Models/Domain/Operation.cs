namespace BookKeeping.Web.Models.Domain
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    ///  Operation
    /// </summary>
    public class Operation
    {
        /// <summary>
        /// Identifier
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Operation category
        /// </summary>
        [Required]        
        public virtual OperationCategory Category { get; set; }

        /// <summary>
        /// Owner of operation
        /// </summary>
        public virtual UserProfile UserProfile { get; set; }

        /// <summary>
        /// Date and time of operation
        /// </summary>
        [Required]
        public DateTime DateTime { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        [Required]
        public decimal Value { get; set; }

        /// <summary>
        /// Comment for operation
        /// </summary>
        public string Comment { get; set; }                                
    }
}