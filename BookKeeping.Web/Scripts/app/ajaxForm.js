﻿$(function() {
    var formSubmitHandler = function(e) {
        var $form = $(this);

        // We check if jQuery.validator exists on the form
        if (!$form.valid || $form.valid()) {

            $.post($form.attr('action'), $form.serializeArray())
                .done(function(json) {
                    json = json || { };
                    $form.trigger("ajaxFormSubmitComplete");
                    if (json.redirect)
                        window.location = json.redirect;
                });
        }

        // Prevent the normal behavior since we opened the dialog
        e.preventDefault();
    };


    $("body").on('submit',"form[ajaxForm = 'True']", formSubmitHandler);
});