﻿window.bookkeeping = window.bookkeeping || {};


(function(o) {
    var addGridViewMixin = function (datacontext, viewModel) {
        var self = viewModel;
        self.rows = ko.observableArray();
        
        self.paging = {
            PageNumber: ko.observable(self.pagingInitData.PageNumber),
            TotalPagesCount: ko.observable(self.pagingInitData.TotalPagesCount),
            pages: ko.observableArray([]),
            next: function() {
                var pn = self.paging.PageNumber();
                if (pn < self.paging.TotalPagesCount())
                    self.paging.PageNumber(pn + 1);
            },
            back: function() {
                var pn = self.paging.PageNumber();
                if (pn > 1)
                    self.paging.PageNumber(pn - 1);
            },
            selectPage: function(pageNumber) {
                self.paging.PageNumber(pageNumber);
            }
        };
        self.selectedAll = ko.computed({
            read: function() {
                if (self.rows().length == 0)
                    return false;
                var firstUnchecked = ko.utils.arrayFirst(self.rows(), function(item) {
                    return item.selected() == false;
                });
                return firstUnchecked == null;
            },
            write: function(value) {
                ko.utils.arrayForEach(self.rows(), function(item) {
                    item.selected(value);
                });
            }
        });

        self.getSelectedRows = function() {
            return ko.utils.arrayFilter(self.rows(), function(item) {
                return item.selected() == true;
            });
        };

        self.hasSelectedRows = ko.computed(function() {
            return self.getSelectedRows().length > 0;
        });

        self.deleteSelected = function() {
            datacontext.deleteItems(self.getSelectedRows()).done(function() {
                self.reload();
            });
        };

        self.reload = function() {
            datacontext.getList(ko.utils.unwrapObservable(self.filter), ko.utils.unwrapObservable(self.paging.PageNumber)).success(
                function(data) {
                    for (var i = 0; i < data.Data.length; i++) {
                        data.Data[i].selected = ko.observable(false);
                    }
                    self.rows(data.Data);
                    self.paging.TotalPagesCount(data.Paging.TotalPagesCount);
                    if (data.Paging.TotalPagesCount < data.Paging.PageNumber) {
                        if (data.Paging.TotalPagesCount > 0) {
                            data.Paging.PageNumber = data.Paging.TotalPagesCount;
                        } else {
                            data.Paging.PageNumber = 1;
                        }
                    }
                    self.paging.PageNumber(data.Paging.PageNumber);
                });
        };

        ko.dependentObservable(function() {
            self.paging.pages.removeAll();
            for (var i = 0; i < self.paging.TotalPagesCount(); i++) {
                self.paging.pages.push(i + 1);
            }
        });

        ko.dependentObservable(function() {
            self.reload();
        }, this);

        ko.dependentObservable(function() {
            var data = ko.utils.unwrapObservable(self.filter);
            self.paging.PageNumber(1);
        }, this);

        return viewModel;
    };

    var gridDatacontext = function(getListUrl, deleteItemsUrl) {

        var datacontext = {
            getList: getList,
            deleteItems: deleteItems
        };

        return datacontext;

        function getList(filterParams, pageNumber) {
            var requestData = { filter: filterParams };
            requestData.pageNumber = pageNumber;
            return ajaxRequest("POST", getListUrl, requestData);
        }

        function deleteItems(items) {
            var idArray = [];
            for (var i = 0; i < items.length; i++) {
                idArray.push(items[i].Id);
            }

            return ajaxRequest("POST", deleteItemsUrl, { id: idArray });
        }

        function ajaxRequest(type, url, data, dataType) {
            var options = {
                dataType: dataType || "json",
                contentType: "application/json",
                cache: false,
                type: type,
                data: data ? ko.toJSON(data) : null                
            };
            return $.ajax(url, options);
        }
    };

    o.AddGridViewMixin = function (viewModel, getListUrl, deleteItemsUrl) {

        var dataContext = new gridDatacontext(getListUrl, deleteItemsUrl);
        var gridModel = addGridViewMixin(dataContext, viewModel);
        return gridModel;
    };
})(window.bookkeeping);
