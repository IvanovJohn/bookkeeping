﻿ko.bindingHandlers.monthpicker = {    
    init: function(element, valueAccessor) {
        var $el = $(element);                       

        $el.datepicker({
            changeMonth: true,
            changeYear: true,            
            dateFormat: 'MM yy',            
            onChangeMonthYear: function (year, month) {                
                var date = new Date(year, month-1, 1);                
                var value = valueAccessor();
                value(date);
            },
            beforeShow: function() {
                $(this).datepicker("widget").addClass("monthpicker");
            },
        });
    }   
};