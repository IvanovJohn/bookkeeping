﻿namespace BookKeeping.Test.Helpers
{
    using System.Linq;

    using BookKeeping.Web.Models.DAL;
    using BookKeeping.Web.Models.Domain;

    internal class OperationCategoryHelper
    {
        public static OperationCategory Create(BookKeepingDataContext dataContext, string identityString)
        {
            var operationCategory = new OperationCategory { Title = identityString };
            dataContext.OperationCategories.Add(operationCategory);
            return operationCategory;
        }

        public static void Delete(BookKeepingDataContext context, string identityString)
        {
            var operationCategories = context.OperationCategories.Where(p => p.Title == identityString);
            foreach (var operationCategory in operationCategories)
            {
                context.OperationCategories.Remove(operationCategory);
            }
        }
    }
}