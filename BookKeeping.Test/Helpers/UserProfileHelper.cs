﻿namespace BookKeeping.Test.Helpers
{
    using System.Linq;

    using BookKeeping.Web.Models.DAL;
    using BookKeeping.Web.Models.Domain;

    internal class UserProfileHelper
    {
        public static UserProfile Create(BookKeepingDataContext dataContext, string identityString)
        {
            var userProfile = new UserProfile { UserName = identityString };
            dataContext.UserProfiles.Add(userProfile);
            return userProfile;
        }

        public static void Delete(BookKeepingDataContext context, string identityString)
        {
            var userProfiles = context.UserProfiles.Where(p => p.UserName == identityString);
            foreach (var userProfile in userProfiles)
            {
                context.UserProfiles.Remove(userProfile);
            }
        }

        public static UserProfile GetById(BookKeepingDataContext context, int userId)
        {
            return context.UserProfiles.First(p => p.UserId == userId);
        }
    }
}