﻿namespace BookKeeping.Test.IntegrationTests
{
    using System;    
    using System.Linq;

    using BookKeeping.Test.Helpers;    
    using BookKeeping.Web.Models.DAL;    

    using NUnit.Framework;

    /// <summary>
    /// Integration test for operations
    /// </summary>
    [TestFixture]
    public class OperationTest
    {
        /// <summary>
        /// User should be able to save a new expense
        /// </summary>
        [Test]
        public void UserCanSaveNewExpense()
        {
            int userId;
            var dt = DateTime.Now.Date;

            // Arrange
            using (var context = new BookKeepingDataContext())
            {
                var testUser = UserProfileHelper.Create(context, "OperationTest");                
                context.SaveChanges();
                userId = testUser.UserId;                
            }

            // Act
            using (var context = new BookKeepingDataContext())
            {
                var testUser = UserProfileHelper.GetById(context, userId);                
                testUser.CreateNewOperation("testCategory", 123, dt, "comment");
                context.SaveChanges();
            }

            // Assert
            using (var context = new BookKeepingDataContext())
            {
                var testUser = UserProfileHelper.GetById(context, userId);
                Assert.AreEqual(1, testUser.Operations.Count());
                var operation = testUser.Operations.First();
                Assert.AreEqual(123, operation.Value);
                Assert.AreEqual("comment", operation.Comment);
                Assert.AreEqual(dt, operation.DateTime);
                Assert.AreEqual("testCategory", operation.Category.Title);
            }
        }
       
        /// <summary>
        /// Tear down
        /// </summary>
        [TearDown]
        public virtual void TearDown()
        {
            this.DeleteDirtyData();
        }
        
        /// <summary>
        /// Fixture set up
        /// </summary>
        [TestFixtureSetUp]
        public virtual void FixtureSetUp()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", TestContext.CurrentContext.WorkDirectory);          

            this.DeleteDirtyData();
        }

        private void DeleteDirtyData()
        {
            using (var context = new BookKeepingDataContext())
            {
                UserProfileHelper.Delete(context, "OperationTest");
                context.SaveChanges();
            }
        }       
    }
}